import * as type from '../type';

export const updateDemoAction = () => {
  let text = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  for (let i = 0; i < 6; i++) text += characters.charAt(Math.floor(Math.random() * characters.length));
  return (
    {
      type: type.UPDATE_DEMO_REDUCER,
      data: { text },
    }
  );
}

import * as type from '../type';

const initState = { text: "", count: 0 };

const demo = (state = initState, action) => {
  switch (action.type) {
    case type.UPDATE_DEMO_REDUCER: {
      let newState = Object.assign({}, state);
      newState = Object.assign({}, newState, action.data);
      newState.count += 1;
      return newState;
    }
    default: {
      return state;
    }
  }
}

export default demo;

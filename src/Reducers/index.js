import { combineReducers } from 'redux';
import demo from './Demo';

const reduxData = combineReducers({
  demo,
});

export default reduxData;

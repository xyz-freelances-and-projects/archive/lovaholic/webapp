import React from 'react';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

import Home from './Views/Home';
import Structure from './Components/Structure';

const Router = () => {
  const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route {...rest}
      render={(props) => true ? <Component {...props} /> : <Redirect to='/' />}
    />
  );
  const AdminRoute = ({ component: Component, ...rest }) => (
    <Route {...rest}
      render={(props) => true ? <Component {...props} /> : <Redirect to='/' />}
    />
  );
  return (
    <BrowserRouter basename={'/'}>
      <Structure>
        <Switch>
          <Route exact path={'/'} component={Home} />
        </Switch>
      </Structure>
    </BrowserRouter>
  );

}
export default Router;

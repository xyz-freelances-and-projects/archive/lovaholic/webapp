import React from 'react';
import { useTranslation } from 'react-i18next';

const Header = () => {
    const { i18n } = useTranslation();
    return (
        <div className="header">
            <span onClick={() => i18n.changeLanguage('en')}>English</span>
            <span onClick={() => i18n.changeLanguage('zh-hk')}>中文</span>
        </div>
    );
}

export default Header;
import React from "react";
import { Box } from 'grommet';
import Header from "./header";

const Structure = ({ children }) => {
  return (
    <Box fill="vertical" border={{ color: 'brand', size: 'large' }} pad='xlarge'>
      {children}
    </Box>
    // <Grid
    //   fill="vertical"
    //   columns={['full']}
    //   rows={['xsmall', 'large', 'xsmall']}
    //   areas={[
    //     { name: 'nav', start: [0, 0], end: [0, 0] },
    //     { name: 'main', start: [0, 1], end: [0, 1] },
    //     { name: 'foot', start: [0, 2], end: [0, 2] },
    //   ]}
    // >
    //   <Box gridArea='nav' background='brand' />
    //   <Box gridArea='main' background='light-2'>
    //     {children}
    //   </Box>
    //   <Box gridArea='foot' background='accent-1' />
    // </Grid>
  );
};

export default Structure;


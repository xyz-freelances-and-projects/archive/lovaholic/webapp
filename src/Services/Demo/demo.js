import service from './../Request';

export function demoService(x, y) {
  const data = {
    x,
    y,
  }

  return service({
    url: '/xyz',
    method: 'post',
    data
  });
}

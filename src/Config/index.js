let baseAPI;

if (process.env.NODE_ENV === 'development') {
  baseAPI = 'https://api6.ipify.org';
} else if (process.env.NODE_ENV === 'production') {
  baseAPI = 'https://api6.ipify.org';
}

export { baseAPI };

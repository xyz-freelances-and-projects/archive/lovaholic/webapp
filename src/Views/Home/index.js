import React from "react";
import { useTranslation } from "react-i18next";
import { useSelector, useDispatch } from "react-redux";
import { updateDemoAction } from "./../../Reducers/Demo/actions";

const Home = () => {
  const { t } = useTranslation();
  const demo = useSelector(state => state.demo);
  const dispatch = useDispatch();
  const updateDemo = () => dispatch(updateDemoAction());

  return (
    <div className="home">
      {`${t("title")} ${demo.text}`}
      <br />
      <button onClick={updateDemo}>{t("button", { count: demo.count })}</button>
    </div>
  );
};

export default React.memo(Home);
